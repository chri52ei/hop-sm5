import { ApolloError } from "@apollo/client";
import { useState } from "react";
import {
  CreateUserRequest,
  CreateUserResponse,
  useCreateUserMutation,
} from "../generated/types";

export const useSignIn = () => {
  const [createUserMutation, { loading }] = useCreateUserMutation({});
  const [state, setState] = useState<CreateUserResponse>({});
  const createUser = (req: CreateUserRequest) => {
    createUserMutation({ variables: { createUserRequest: req } })
      .then((res) => {
        if (res.data) setState(res.data.createUser);
      })
      .catch((err: ApolloError) => {
        setState({ errors: [err.message] });
      });
  };
  return {
    createUser,
    state,
    loading,
  };
};
