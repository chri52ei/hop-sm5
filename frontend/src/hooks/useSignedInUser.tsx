import { useCurrentUserQuery } from "../generated/types";

export const useSignedInUser = () => {
  const { data, error, loading } = useCurrentUserQuery({ pollInterval: 500 });

  return {
    user: data?.currentUser.user,
    error: error?.message,
    loading,
  };
};
