import { createContext, useContext } from "react";
import defaultTheme, { Theme } from "../config/theme";

const ThemeContext = createContext<Theme>(defaultTheme);

export default function useTheme(): Theme {
  return useContext(ThemeContext);
}

export function ThemeProvider({
  children,
  theme,
}: {
  children: JSX.Element;
  theme: Theme;
}): JSX.Element {
  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
}
