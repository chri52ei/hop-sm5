import { useState } from "react";
import { SignOutResponse, useSignOutMutation } from "../generated/types";

export const useSignOut = () => {
  const [signOutMutation, { data, error, loading }] = useSignOutMutation();
  const [state, setState] = useState<SignOutResponse>({ success: false });

  const signOut = () => {
    return signOutMutation().then((res) => {
      if (res.data) {
        setState(res.data.signOut);
      }
    });
  };

  return {
    signOut,
    state,
    loading
  };
};
