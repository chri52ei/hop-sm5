import { ApolloError } from "@apollo/client";
import { useState } from "react";
import {
  SignInRequest,
  SignInResponse,
  useSignInMutation,
} from "../generated/types";

export const useSignIn = () => {
  const [signInMutation, { loading }] = useSignInMutation();
  const [state, setState] = useState<SignInResponse>({});
  const signIn = (req: SignInRequest) => {
    signInMutation({ variables: { req } })
      .then((res) => {
        if (res.data) setState(res.data.signIn);
      })
      .catch((err: ApolloError) => {
        setState({ errors: [err.message] });
      });
  };
  return {
    signIn,
    state,
    loading,
  };
};
