import { Input } from "semantic-ui-react";

interface props {
  label: string;
  type: "text" | "password" | "number";
  name: string;
  errors: string[] | undefined;
}

const FormInput: React.FC<props> = ({ label, type, name, errors }) => {
  return (
    <div className="root">
      <label>{label}</label>
      <Input type={type} name={name} />
      {errors?.map((e) => (
        <label key={e} className="error">
          {e}
        </label>
      ))}
      <style jsx>{`
        .root {
          display: flex;
          flex-direction: column;
          margin: 10px 0px;
        }
        .root label {
          color: rgba(0, 0, 0, 0.87);
          font-weight: 700;
          font-size: 14px;
          margin-bottom: 4px;
        }
        .error {
          color: red;
        }
      `}</style>
    </div>
  );
};

export default FormInput;
