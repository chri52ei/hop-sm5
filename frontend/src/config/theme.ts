export type Theme = typeof defaultTheme;

const defaultTheme = {
  media: {
    mobile: 1000,
    small: 1366,
    medium: 1600,
    large: 1920,
  },
  colors: {
    primary: "#2A5EF6",
    primaryLight: "#4d6d78",
    primaryDisabled: "#b4b4b4",
    abyssLighter: "#054463",
    abyssPlaceholder: "#4d6c78",

    // Primary button style
    primaryButtonBackground: "#0D5B8B",
    primaryButtonActiveBackground: "#3689BC",
    primaryButtonText: "#FFF",
    secondaryButtonBackground: "#f5f7f8",
    secondaryButtonActiveBackground: "#dde3e6",

    // Notification
    white: "#f9fafc",
    green: "#00a86b",
    red: "#ff6f61",
    yellow: "#fdda7e",
    blue: "#0a5b8b",
    success: "#00a86b",
    error: "#ff6f61",
    warning: "#fdda7e",
    info: "#0a5b8b",
    lightgrey: "#e5eaeb",
    primaryText: "#012d3e",

    // Status
    Online: "#00a86b",
    Pending: "#fdda7e",
    Draft: "#ff6f61",
    Finished: "#00a86b",
    OnlineBackground: "#e5f6f0",
    ClosedBackground: "rgba(255, 111, 97, 0.05)",
  },
  padding: {
    small: 8,
    normal: 16,
    medium: 32,
    large: 64,
  },
  margin: {
    small: 8,
    normal: 16,
    medium: 32,
    large: 64,
  },
  topbar: {
    height: 60,
  },
  sidebar: {
    width: 65,
  },
};

export default defaultTheme;
