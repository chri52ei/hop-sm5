export const phoneCodes = [
  {
    value: "+7 840",
    text: "+7 840",
    key: '"+7 840',
  },
  {
    value: "+45",
    text: "+45",
    key: '"+45',
  },
  {
    value: "+90",
    text: "+90",
    key: '"+90',
  },
];
