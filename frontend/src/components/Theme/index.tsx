import React, { useState, useRef } from "react";
import useTheme from "../../hooks/useTheme";
import SideBar from "../Sidebar";
import Content from "../Content";

export default function Theme({
  children,
}: {
  children: JSX.Element;
}): JSX.Element {
  const contentFocusInput = useRef<HTMLInputElement>(null);
  const theme = useTheme();

  return (
    <div className="root">
      <SideBar />
      <div className="wrapper">
        <Content setRef={contentFocusInput}>{children}</Content>
      </div>

      <style jsx>{`
        .root {
          display: flex;
          width: 100%;
          height: 100%;
        }

        .root > .wrapper {
          position: relative;
          display: flex;
          width: 100%;
          height: 100%;
        }

        @media only screen and (max-width: ${theme.media.mobile}px) {
          .root > .wrapper {
            padding-top: 70px;
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  );
}
