import { NextPage } from "next";

import { LoginForm } from "./LoginForm";

const LoginPage: NextPage = () => {
  return <LoginForm />;
};

export default LoginPage;
