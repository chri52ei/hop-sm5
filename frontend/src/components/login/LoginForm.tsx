import { useSignIn } from "../../hooks/useSignIn";
import { Button, Input } from "semantic-ui-react";
import FormInput from "../../ui/FormInput";
import { Form } from "../../components/Form";

export const LoginForm: React.FC = () => {
  const { signIn, state, loading } = useSignIn();

  return (
    <div className="root">
      <div className="image">
        <img alt="logo" src="images/icons/logo_black.svg" />
      </div>
      <Form
        values={{ username: "", password: "" }}
        onSubmit={(values) => signIn(values)}
      >
        {({ fields }) => (
          <>
            {loading ? <p>Loading</p> : <></>}
            {state.errors?.map((e) => (
              <label key={e}>{e}</label>
            ))}
            <FormInput
              label="Brugernavn"
              type="text"
              {...fields.username}
              errors={state?.username?.errors}
            />
            <FormInput
              label="Kode"
              type="password"
              {...fields.password}
              errors={state?.password?.errors}
            />
            <Button
              color="blue"
              type="submit"
              value="login"
              style={{ width: "100%" }}
            >
              Login
            </Button>
          </>
        )}
      </Form>
      <style jsx>
        {`
          .root {
            width: 300px;
            margin: auto;
          }
          img {
            width: 100%;
          }
        `}
      </style>
    </div>
  );
};
