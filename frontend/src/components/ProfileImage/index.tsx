import Theme from "../Theme";

export default function ProfileImage({
  image,
  name,
}: {
  image: string;
  name: string;
}): JSX.Element {
  return (
    <div className="root">
      <div className="image-wrapper">
        <img alt="profile-image" className="profile-image" src={image} />
        <p>{name}</p>
      </div>
      <style jsx>{`
        .image-wrapper {
          display: flex;
          flex-direction: column;
          padding: 40px 0px;
          align-items: center;
          justify-content: center;
        }
        .profile-image {
          border-radius: 100%;
          height: 50px;
          width: 50px;
          margin-bottom: 10px;
          object-fit: cover;
        }
      `}</style>
    </div>
  );
}
