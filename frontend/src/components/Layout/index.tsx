import Head from "next/head";
import Theme from "../Theme";
import LoginPage from "../login";
import { useSignedInUser } from "../../hooks/useSignedInUser";

export default function Layout({
  children,
}: {
  children: JSX.Element;
}): JSX.Element {
  const { user } = useSignedInUser();
  console.log(user);

  return (
    <div className="root">
      {user ? <Theme>{children}</Theme> : <LoginPage />}
      <style jsx>{`
        .root {
          display: flex;
          width: 100%;
          height: 100%;
        }
      `}</style>
    </div>
  );
}
