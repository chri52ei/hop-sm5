import React from "react";
import useTheme from "../../hooks/useTheme";
import ProfileImage from "../ProfileImage";
import { useSignOut } from "../../hooks/useSignOut";
import { useSignedInUser } from "../../hooks/useSignedInUser";
import {
  AiFillHome,
  AiFillDollarCircle,
  AiOutlineTeam,
  AiOutlineLogout,
} from "react-icons/ai";
import Link from "next/link";
import { Icon } from "semantic-ui-react";
import {
  ProSidebar,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
  Menu,
  MenuItem,
  SubMenu,
} from "react-pro-sidebar";
import "react-pro-sidebar/dist/css/styles.css";

const SidebarExampleVisible = () => {
  const theme = useTheme();

  const { user } = useSignedInUser();

  const { signOut, state, loading } = useSignOut();
  return (
    <div>
      <ProSidebar style={{ color: "#F3F2EF", minWidth: "0", width: "200px" }}>
        <SidebarHeader>
          <div className="container">
            <img
              className="logo"
              alt="logo"
              src="/images/icons/logo_beige.svg"
            />
          </div>
        </SidebarHeader>
        <SidebarContent>
          <ProfileImage
            image="/images/test.jpg"
            name={user?.username ?? "Brugernavn ikke angivet"}
          />
          <Menu iconShape="square">
            <MenuItem title="" icon={<AiFillHome />}>
              <Link href="/">
                <p>Dashboard</p>
              </Link>
            </MenuItem>
            <MenuItem icon={<AiOutlineTeam />}>
              <Link href="/customers">
                <p>Kunder</p>
              </Link>
            </MenuItem>
            <SubMenu icon={<AiFillDollarCircle />} title="Økonomi">
              <MenuItem>
                <Link href="/invoices">
                  <p>Fakturaer</p>
                </Link>
              </MenuItem>
              <MenuItem>
                <Link href="/subscriptions">
                  <p>Abonnementer</p>
                </Link>
              </MenuItem>
              <MenuItem>
                <Link href="/plans">
                  <p>Planer</p>
                </Link>
              </MenuItem>
            </SubMenu>
          </Menu>
        </SidebarContent>
        <Menu iconShape="square">
          <MenuItem onClick={() => signOut()} icon={<AiOutlineLogout />}>
            Logout
          </MenuItem>
        </Menu>
        <SidebarFooter>
          <div className="container">
            <p>© 2021 SM5 All Rights Reserved</p>
          </div>
        </SidebarFooter>
      </ProSidebar>
      <style jsx>{`
        .container {
          padding: 20px;
        }
        .logo {
          width: 100%;
        }
      `}</style>
    </div>
  );
};

export default SidebarExampleVisible;
