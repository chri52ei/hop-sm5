import React from "react";
import useTheme from "../../hooks/useTheme";

interface ContentProps {
  children: JSX.Element;
  setRef: React.RefObject<HTMLInputElement>;
}

export default function Content({
  children,
  setRef,
}: ContentProps): JSX.Element {
  const theme = useTheme();
  // setRef used for tabbing to content area with the 'to centen' button (accessibility).
  return (
    <main className="root" role="main" aria-label="Content area">
      <div>
        <input ref={setRef} type="text" className="to-content-input" />
        {children}
      </div>

      <style jsx>{`
        .root {
          width: 100%;
          height: 100%;
          overflow: hidden;
          overflow-y: auto;
        }

        .root > div {
          width: 100%;
          height: auto;
          padding: ${theme.padding.normal == null
            ? "16"
            : theme.padding.normal}px;
        }

        // Hidden input for tab to content.
        .root .to-content-input {
          position: absolute;
          top: -100%;
          left: -100%;
          width: 0px;
          height: 0px;
          border: none;
          outline: none;
          opacity: 0;
          filter: alpha(opacity=0);
          overflow: hidden;
        }

        // Scrollbar
        .root::-webkit-scrollbar {
          width: 8px;
        }

        .root::-webkit-scrollbar-track {
          background-color: rgba(0, 0, 0, 0.15);
        }

        .root::-webkit-scrollbar-thumb {
          background-color: rgba(0, 0, 0, 0.25);
          border-radius: 4px;
        }

        .root::-webkit-scrollbar-thumb:hover {
        }
      `}</style>
    </main>
  );
}
