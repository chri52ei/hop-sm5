import React, { FormEvent, ReactElement } from "react";

type AnyObject = {
  [key: string]: any;
};
type FormFieldProps = {
  name: string;
};

type FormChildProps<T> = {
  fields: {
    [Key in keyof T]: FormFieldProps;
  };
};
type FormValues<T> = {
  [Key in keyof T]: T[Key];
};

type FormBuilderProps<T> = {
  values: FormValues<T>;
  onSubmit: (args: FormValues<T>) => void;
  children(props: FormChildProps<T>): ReactElement;
};
export function Form<T>(props: FormBuilderProps<T>) {
  const { children } = props;

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const submitArgs: AnyObject = {};

    for (let i = 0; i < e.currentTarget.elements.length; i++) {
      const element: typeof e.currentTarget.elements[0] & {
        name?: string;
        value?: string;
      } = e.currentTarget.elements[i];

      for (const [key, _] of Object.entries(props.values)) {
        if (element.name == key) {
          submitArgs[key] = element.value;
        }
      }
    }
    props.onSubmit(submitArgs as FormValues<T>);
  };

  const childProps: { fields: AnyObject } = { fields: {} };
  for (const [key, _] of Object.entries(props.values)) {
    childProps.fields[key] = { errors: [], name: key };
  }

  return (
    <form onSubmit={handleSubmit}>
      {children(childProps as FormChildProps<T>)}
    </form>
  );
}
