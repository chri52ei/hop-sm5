import { Button, Form, Input } from "formik-semantic-ui";
import { SetStateAction } from "react";
import { countries } from "../../config/countries";
import { phoneCodes } from "../../config/phoneCodes";
import { useState } from "react";
import { Header, Dropdown } from "semantic-ui-react";

const methods = [
  { key: "netto", text: "Netto", value: "netto" },
  { key: "running", text: "Løbende måned", value: "running" },
];

type props = {
  setOpen: (value: SetStateAction<boolean>) => void;
};

export const NewDineroCustomer = ({ setOpen }: props) => {
  const [type, setType] = useState("firm");
  const options = [
    { key: "firm", text: "Firma", value: "firm" },
    { ey: "private", text: "Privatperson", value: "private" },
  ];

  interface MyFormValues {
    name: string;
    surname: string;
    firm: string;
    vat: string;
    email: string;
    phone: string;
    address: string;
    cvr: string;
    zip: string;
    city: string;
    country: string;
    website: string;
    att: string;
    ean: string;
    days: number;
  }

  const initialValues: MyFormValues = {
    name: "",
    surname: "",
    firm: "",
    vat: "",
    email: "",
    phone: "",
    address: "",
    cvr: "",
    zip: "",
    city: "",
    country: "",
    website: "",
    att: "",
    ean: "",
    days: 0,
  };
  return (
    <div className="root">
      <Form
        initialValues={initialValues}
        onSubmit={(
          {
            name,
            surname,
            firm,
            vat,
            email,
            phone,
            address,
            cvr,
            zip,
            city,
            website,
            att,
            ean,
            days,
          },
          { setSubmitting }
        ) => {
          console.log(
            name,
            surname,
            firm,
            vat,
            email,
            phone,
            address,
            cvr,
            zip,
            city,
            website,
            att,
            ean,
            days
          );

          setSubmitting(false);
          setOpen(false);
        }}
      >
        <div className="header">
          <Header as="h3" style={{ marginTop: "15px" }}>
            {type == "firm" ? "Firmadetaljer" : "Persondetljer"}
          </Header>

          <Dropdown
            options={options}
            value={type}
            onChange={(e, item) => setType(item.value)}
          />
        </div>
        <Input label={type == "firm" ? "Firmanavn" : "Navn"} name="name" />
        <Input label="Adresse" name="address" />
        <div className="fields">
          <Input label="Post nr." name="zip" />
          <Input label="By" name="city" />
        </div>
        <div className="country">
          <Dropdown
            style={{ width: "100%" }}
            placeholder="Vælg land"
            selection
            options={countries}
          />
        </div>
        {type == "firm" && <Input label="CVR Nr." name="cvr" />}
        <Header as="h3">Kontaktoplysninger</Header>
        <Input label="Email" name="email" />
        <div className="phone">
          <Dropdown placeholder="landekode" selection options={phoneCodes} />
          <Input label="telefon" name="phone" />
        </div>
        {type == "firm" && (
          <>
            <div className="fields">
              <Input label="Hjemmeside" name="website" />
              <Input label="Att. person" name="att" />
            </div>
            <Input label="EAN Nr." name="ean" />
          </>
        )}
        <Header as="h3">Betalingsbetingelser</Header>
        <div className="phone">
          <Dropdown placeholder="Metode" selection options={methods} />
          <Input label="Dage" name="days" />
        </div>
        <div className="buttons">
          <Button onClick={() => setOpen(false)}>Cancel</Button>
          <Button.Submit>Submit</Button.Submit>
        </div>
      </Form>
      <style jsx>{`
        .header {
          display: flex;
          justify-content: space-between;
        }
        .root {
          display: flex;
          flex-direction: column;
        }
        .fields {
          display: flex;
          justify-content: space-between;
        }
        .fields :global(input) {
          width: 400px !important;
        }
        .phone {
          display: flex;
          justify-content: space-between;
          align-items: end;
          margin-bottom: 20px;
        }
        .phone :global(input) {
          width: 600px !important;
        }
        .country {
          margin-bottom: 10px;
        }
        .buttons {
          display: flex;
          justify-content: space-between;
          margin-top: 15px;
        }
      `}</style>
    </div>
  );
};
