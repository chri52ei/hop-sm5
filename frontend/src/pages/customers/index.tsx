import { NextPage } from "next";
import _, { create } from "lodash";
import { NewReepayCustomer } from "./newReepayCustomer";
import { NewDineroCustomer } from "./newDineroCustomer";

import {
  Icon,
  Label,
  Menu,
  Table,
  Search,
  Button,
  Header,
  Modal,
  Dropdown,
} from "semantic-ui-react";
import { useReducer, useState } from "react";

type customer = {
  name: string;
  address: string;
  email: string;
  country: string;
  phone: string;
};

const Data = [
  {
    name: "Test firma",
    address: "testvej 21",
    email: "testfirma@gmail.com",
    country: "DK",
    phone: "88888888",
  },
  {
    name: "Macdonalds",
    address: "mcdvej 21",
    email: "mcd@gmail.com",
    country: "US",
    phone: "12312312",
  },
  {
    name: "Maersk",
    address: "Maerskvej 21",
    email: "maesk@gmail.com",
    country: "DK",
    phone: "15141241",
  },
  {
    name: "Test firma 2",
    address: "testfirmavej 21",
    email: "testfirma2@gmail.com",
    country: "DK",
    phone: "12345677",
  },
];

function exampleReducer(state: any, action: any) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      }

      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: "ascending",
      };
    default:
      throw new Error();
  }
}

const Customers: NextPage = () => {
  const [state, dispatch] = useReducer(exampleReducer, {
    column: null,
    data: Data,
    direction: null,
  });
  const { column, data, direction } = state;

  const [open, setOpen] = useState(false);
  const [createCustomer, setCreateCustomer] = useState<string>("reepay");
  const options = [
    { key: "reepay", text: "Reepay kunde", value: "reepay" },
    { key: "dinero", text: "Dinero kunde", value: "dinero" },
  ];
  return (
    <div>
      <h1>Kunder</h1>
      <div className="top">
        <Search />
        <Modal
          onClose={() => setOpen(false)}
          onOpen={() => setOpen(true)}
          open={open}
          trigger={<Button color="blue">Ny kunde</Button>}
        >
          <Modal.Header>Create a new customer</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Dropdown
                options={options}
                value={createCustomer}
                selection
                onChange={(e, item) => setCreateCustomer(item.value)}
              />
              {createCustomer == "reepay" ? (
                <NewReepayCustomer setOpen={setOpen} />
              ) : (
                <NewDineroCustomer setOpen={setOpen} />
              )}
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </div>
      <Table celled sortable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell
              sorted={column === "name" ? direction : null}
              onClick={() => dispatch({ type: "CHANGE_SORT", column: "name" })}
            >
              Navn
            </Table.HeaderCell>
            <Table.HeaderCell>Adresse</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Land</Table.HeaderCell>
            <Table.HeaderCell>Mobil</Table.HeaderCell>
            <Table.HeaderCell>Edit</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((customer: customer, i: number) => (
            <Table.Row key={i}>
              <Table.Cell>{customer.name}</Table.Cell>
              <Table.Cell>{customer.address}</Table.Cell>
              <Table.Cell>{customer.email}</Table.Cell>
              <Table.Cell>{customer.country}</Table.Cell>
              <Table.Cell>{customer.phone}</Table.Cell>
              <Table.Cell selectable textAlign="center">
                <a onClick={() => console.log(customer.name)}>
                  <Icon name="ellipsis horizontal" />
                </a>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="6">
              <Menu floated="right" pagination>
                <Menu.Item as="a" icon>
                  <Icon name="chevron left" />
                </Menu.Item>
                <Menu.Item as="a">1</Menu.Item>
                <Menu.Item as="a">2</Menu.Item>
                <Menu.Item as="a">3</Menu.Item>
                <Menu.Item as="a">4</Menu.Item>
                <Menu.Item as="a" icon>
                  <Icon name="chevron right" />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <style jsx>{`
        .top {
          display: flex;
          justify-content: space-between;
        }
        a {
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Customers;
