import { Button, Form, Input } from "formik-semantic-ui";
import { Header, Dropdown } from "semantic-ui-react";
import { countries } from "../../config/countries";
import { phoneCodes } from "../../config/phoneCodes";
import { SetStateAction } from "react";

type props = {
  setOpen: (value: SetStateAction<boolean>) => void;
};

export const NewReepayCustomer = ({ setOpen }: props) => {
  return (
    <div className="root">
      <Form
        initialValues={{
          name: "",
          surname: "",
          firm: "",
          vat: "",
          email: "",
          phone: "",
          address: "",
          address2: "",
          zip: "",
          city: "",
          country: "",
        }}
        onSubmit={(
          {
            name,
            surname,
            firm,
            vat,
            email,
            phone,
            address,
            address2,
            zip,
            city,
          },
          { setSubmitting }
        ) => {
          console.log(
            name,
            surname,
            firm,
            vat,
            email,
            phone,
            address,
            address2,
            zip,
            city
          );

          setSubmitting(false);
          setOpen(false);
        }}
      >
        <Header style={{ marginTop: "15px" }} as="h3">
          Kundeinformation
        </Header>
        <div className="fields">
          <Input label="Navn" name="name" />
          <Input label="Efternavn" name="surname" />
        </div>
        <div className="fields">
          <Input label="firma" name="firm" />
          <Input label="VAT nr." name="vat" />
        </div>
        <Header as="h3">Kontaktoplysninger</Header>
        <Input label="Email" name="email" />
        <div className="phone">
          <Dropdown placeholder="landekode" selection options={phoneCodes} />
          <Input label="telefon" name="phone" />
        </div>
        <Header as="h3">Adresse information</Header>
        <Input label="Adresse" name="address" />
        <Input label="Adresse 2" name="address2" />
        <div className="fields">
          <Input label="Post nr." name="zip" />
          <Input label="By" name="city" />
        </div>
        <div className="country">
          <Dropdown
            style={{ width: "100%" }}
            placeholder="Vælg land"
            selection
            options={countries}
          />
        </div>
        <div className="buttons">
          <Button onClick={() => setOpen(false)}>Cancel</Button>
          <Button.Submit>Submit</Button.Submit>
        </div>
      </Form>
      <style jsx>{`
        .root {
          display: flex;
          flex-direction: column;
        }
        .fields {
          display: flex;
          justify-content: space-between;
        }
        .fields :global(input) {
          width: 400px !important;
        }
        .phone {
          display: flex;
          justify-content: space-between;
          align-items: end;
        }
        .phone :global(input) {
          width: 600px !important;
        }
        .buttons {
          display: flex;
          justify-content: space-between;
          margin-top: 15px;
        }
      `}</style>
    </div>
  );
};
