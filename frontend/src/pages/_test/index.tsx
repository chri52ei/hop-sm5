import type { NextPage } from "next";
import { Layout } from "../../components/Layout";
import { SignedInOnly } from "../../components/SignedInOnly";
import { useSignedInUser } from "../../hooks/useSignedInUser";

const TestingPage: NextPage = () => {
  const { user } = useSignedInUser();
  return (
    <>
      <SignedInOnly>
        <Layout>Hello {user?.username}</Layout>
      </SignedInOnly>
    </>
  );
};

export default TestingPage;
