import { NextPage } from "next";
import _ from "lodash";
import { Icon, Label, Menu, Table, Search, Button } from "semantic-ui-react";
import { useReducer } from "react";

type invoice = {
  number: number;
  status: string;
  customer: string;
  description: string;
  dueDate: string;
  amount: number;
};

const Data = [
  {
    number: 1,
    status: "Booked",
    customer: "test firma",
    description: "faktura, ikke sendt",
    dueDate: "2021-06-12",
    amount: 1000,
  },
  {
    number: 2,
    status: "Draft",
    customer: "test firma",
    description: "faktura, ikke sendt",
    dueDate: "2021-06-12",
    amount: 1500,
  },
  {
    number: 3,
    status: "Settled",
    customer: "test firma",
    description: "inv-310",
    dueDate: "2021-06-12",
    amount: 10000,
  },
  {
    number: 4,
    status: "Pending",
    customer: "test firma",
    description: "inv-311",
    dueDate: "2021-06-12",
    amount: 12000,
  },
];

function exampleReducer(state: any, action: any) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      }

      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: "ascending",
      };
    default:
      throw new Error();
  }
}

const Invoices: NextPage = () => {
  const [state, dispatch] = useReducer(exampleReducer, {
    column: null,
    data: Data,
    direction: null,
  });
  const { column, data, direction } = state;
  return (
    <div>
      <h1>Fakturaer</h1>
      <div className="top">
        <Search />
        <Button color="blue">Ny faktura</Button>
      </div>
      <Table celled sortable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell
              sorted={column === "number" ? direction : null}
              onClick={() =>
                dispatch({ type: "CHANGE_SORT", column: "number" })
              }
            >
              Nr.
            </Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell>Kunde</Table.HeaderCell>
            <Table.HeaderCell>Beskrivelse</Table.HeaderCell>
            <Table.HeaderCell>Forfaldsdato</Table.HeaderCell>
            <Table.HeaderCell>Pris</Table.HeaderCell>
            <Table.HeaderCell>Edit</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((invoice: invoice, i: number) => (
            <Table.Row key={i}>
              <Table.Cell>{invoice.number}</Table.Cell>
              <Table.Cell>{invoice.status}</Table.Cell>
              <Table.Cell>{invoice.customer}</Table.Cell>
              <Table.Cell>{invoice.description}</Table.Cell>
              <Table.Cell>{invoice.dueDate}</Table.Cell>
              <Table.Cell>{invoice.amount}</Table.Cell>
              <Table.Cell selectable textAlign="center">
                <a onClick={() => console.log(invoice.number)}>
                  <Icon name="ellipsis horizontal" />
                </a>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="7">
              <Menu floated="right" pagination>
                <Menu.Item as="a" icon>
                  <Icon name="chevron left" />
                </Menu.Item>
                <Menu.Item as="a">1</Menu.Item>
                <Menu.Item as="a">2</Menu.Item>
                <Menu.Item as="a">3</Menu.Item>
                <Menu.Item as="a">4</Menu.Item>
                <Menu.Item as="a" icon>
                  <Icon name="chevron right" />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <style jsx>{`
        .top {
          display: flex;
          justify-content: space-between;
        }
        a {
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Invoices;
