import { NextPage } from "next";
import _ from "lodash";
import { Icon, Label, Menu, Table, Search, Button } from "semantic-ui-react";
import { useReducer } from "react";

type subscription = {
  status: string;
  plan: string;
  title: string;
  created: string;
  nextInvoice: string;
};

const Data = [
  {
    status: "Settled",
    plan: "plan-1",
    title: "sub-001",
    created: "2021-06-12",
    nextInvoice: "2022-06-01",
  },
  {
    status: "Settled",
    plan: "plan-2",
    title: "sub-002",
    created: "2021-06-12",
    nextInvoice: "2022-06-01",
  },
  {
    status: "Settled",
    plan: "plan-3",
    title: "sub-003",
    created: "2021-06-12",
    nextInvoice: "2022-06-01",
  },
  {
    status: "cancelled",
    plan: "plan-4",
    title: "sub-004",
    created: "2021-06-12",
    nextInvoice: "2022-06-01",
  },
];

function exampleReducer(state: any, action: any) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      }

      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: "ascending",
      };
    default:
      throw new Error();
  }
}

const Subscriptions: NextPage = () => {
  const [state, dispatch] = useReducer(exampleReducer, {
    column: null,
    data: Data,
    direction: null,
  });
  const { column, data, direction } = state;
  return (
    <div>
      <h1>Abonnementer</h1>
      <div className="top">
        <Search />
        <Button color="blue">Nyt abonnement</Button>
      </div>
      <Table celled sortable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell
              sorted={column === "number" ? direction : null}
              onClick={() =>
                dispatch({ type: "CHANGE_SORT", column: "number" })
              }
            >
              Titel
            </Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell>Plan</Table.HeaderCell>
            <Table.HeaderCell>Dato</Table.HeaderCell>
            <Table.HeaderCell>Næste faktura</Table.HeaderCell>
            <Table.HeaderCell>Edit</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((subscription: subscription, i: number) => (
            <Table.Row key={i}>
              <Table.Cell>{subscription.title}</Table.Cell>
              <Table.Cell>{subscription.status}</Table.Cell>
              <Table.Cell>{subscription.plan}</Table.Cell>
              <Table.Cell>{subscription.created}</Table.Cell>
              <Table.Cell>{subscription.nextInvoice}</Table.Cell>
              <Table.Cell selectable textAlign="center">
                <a onClick={() => console.log(subscription.title)}>
                  <Icon name="ellipsis horizontal" />
                </a>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="6">
              <Menu floated="right" pagination>
                <Menu.Item as="a" icon>
                  <Icon name="chevron left" />
                </Menu.Item>
                <Menu.Item as="a">1</Menu.Item>
                <Menu.Item as="a">2</Menu.Item>
                <Menu.Item as="a">3</Menu.Item>
                <Menu.Item as="a">4</Menu.Item>
                <Menu.Item as="a" icon>
                  <Icon name="chevron right" />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <style jsx>{`
        .top {
          display: flex;
          justify-content: space-between;
        }
        a {
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Subscriptions;
