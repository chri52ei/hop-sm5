import { NextPage } from "next";
import _ from "lodash";
import { Icon, Label, Menu, Table, Search, Button } from "semantic-ui-react";
import { useReducer } from "react";

type plans = {
  status: string;
  name: string;
  id: string;
  type: string;
  amount: number;
};

const Data = [
  {
    status: "Settled",
    name: "plan-1",
    id: "planId-1",
    type: "Month startdate",
    amount: 6500,
  },
  {
    status: "Settled",
    name: "plan-2",
    id: "planId-2",
    type: "Month startdate",
    amount: 2500,
  },
  {
    status: "Settled",
    name: "plan-3",
    id: "planId-3",
    type: "Week startdate",
    amount: 500,
  },
  {
    status: "cancelled",
    name: "plan-4",
    id: "planId-4",
    type: "Week startdate",
    amount: 200,
  },
];

function exampleReducer(state: any, action: any) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      }

      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: "ascending",
      };
    default:
      throw new Error();
  }
}

const Plans: NextPage = () => {
  const [state, dispatch] = useReducer(exampleReducer, {
    column: null,
    data: Data,
    direction: null,
  });
  const { column, data, direction } = state;
  return (
    <div>
      <h1>Planer</h1>
      <div className="top">
        <Search />
        <Button color="blue">Ny plan</Button>
      </div>
      <Table celled sortable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Status</Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === "name" ? direction : null}
              onClick={() => dispatch({ type: "CHANGE_SORT", column: "name" })}
            >
              Name
            </Table.HeaderCell>
            <Table.HeaderCell>Id</Table.HeaderCell>
            <Table.HeaderCell>Type faktura</Table.HeaderCell>
            <Table.HeaderCell>Pris</Table.HeaderCell>
            <Table.HeaderCell>Edit</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((subscription: plans, i: number) => (
            <Table.Row key={i}>
              <Table.Cell>{subscription.status}</Table.Cell>
              <Table.Cell>{subscription.name}</Table.Cell>
              <Table.Cell>{subscription.id}</Table.Cell>
              <Table.Cell>{subscription.type}</Table.Cell>
              <Table.Cell>{subscription.amount}</Table.Cell>
              <Table.Cell selectable textAlign="center">
                <a onClick={() => console.log(subscription.name)}>
                  <Icon name="ellipsis horizontal" />
                </a>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="6">
              <Menu floated="right" pagination>
                <Menu.Item as="a" icon>
                  <Icon name="chevron left" />
                </Menu.Item>
                <Menu.Item as="a">1</Menu.Item>
                <Menu.Item as="a">2</Menu.Item>
                <Menu.Item as="a">3</Menu.Item>
                <Menu.Item as="a">4</Menu.Item>
                <Menu.Item as="a" icon>
                  <Icon name="chevron right" />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <style jsx>{`
        .top {
          display: flex;
          justify-content: space-between;
        }
        a {
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Plans;
