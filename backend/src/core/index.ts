import { Field, ObjectType } from "type-graphql";

export interface BaseStore<T> {
  find(where: Partial<T>): Promise<T | undefined>;
  create(entity: T): Promise<T | undefined>;
  delete(where: Partial<T>): Promise<boolean>;
  findMany(where: Partial<T>): Promise<T[]>;
  update(where: Partial<T>, action: (obj: T) => void): Promise<boolean>;
}

export interface Database {
  getTable<T>(name: string): Promise<BaseStore<T>>;
}

@ObjectType()
export class ErrorList {
  @Field(() => [String])
  errors!: string[];
}

export type ToResponse<T> = {
  [Key in keyof T]?: ErrorList;
} & Partial<ErrorList> & { success?: boolean };
