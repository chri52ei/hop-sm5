declare namespace NodeJS {
  export interface ProcessEnv {
    MODE: string;
    SESSION_SECRET: string;
    FRONTEND_URL: string;
    PORT:number;
    Dinero_Base_Url: string;
    Dinero_Client_Id: string;
    Dinero_Client_Secret: string;
    Dinero_Auth_Url: string;
    Dinero_Access_Token_Url: string;
    Dinero_Api_Scopes: string;
    Dinero_Organization_Id: string;
  }
}
