import "reflect-metadata";
import { AdminService } from "./domain/services/AdminService";
import { AuthService } from "./domain/services/AuthService";
import { Container, ContainerInstance } from "typedi";
import { MemoryStore } from "./infrastructure/memorystore";
import { User } from "./domain/models/User";
import { UserContext } from "./domain/contexts";
import { ApolloServer } from "apollo-server-express";
import { buildSchema, ResolverData } from "type-graphql";
import * as dotenv from "dotenv";
import express from "express";
import sessions from "express-session";
import { GraphQLRequestContext } from "apollo-server-core";
import cors from "cors";

dotenv.config();

(async () => {
  const app = express();
  let ids = 0;

  let userStore = new MemoryStore<User>();
  await userStore.create({ username: "admin", password: "password" });

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(
    cors({
      origin: [process.env.FRONTEND_URL!, "https://studio.apollographql.com"],
      credentials: true,
    })
  );
  app.use(
    sessions({
      secret: process.env.SESSION_SECRET!,
      saveUninitialized: true,
      cookie: { maxAge: 1000 * 60 * 60 * 24 },
      resave: false,
    })
  );
  app.use(async (req, res, next) => {
    if (!Container.has(req.session.id)) {
      const container = Container.of(req.session.id);
      container.set("UserStore", userStore);
      container.set("UserContext", {});
    }
    //TODO: Reset container to prevent memory leaks
    //res.on("finish", () => Container.reset(req.session.id));
    next();
  });

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [AuthService, AdminService],
      container: ({ context }) => context.container,
      validate: false,
    }),
    context: async ({ req }) => {
      const container = Container.of(req.session.id); // get scoped container
      return { id: req.session.id, container };
    },
  });

  await apolloServer.start();
  apolloServer.applyMiddleware({ app, cors: false });

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000`)
  );
})();
