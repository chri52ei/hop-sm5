import { User } from "../models/User";

export interface UserContext {
    currentUser?: User | undefined;
}
