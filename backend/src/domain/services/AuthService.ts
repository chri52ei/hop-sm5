import { Arg, Mutation, Query, Resolver } from "type-graphql";
import { Inject, Service } from "typedi";
import { UserContext } from "../contexts/UserContext";
import { UserStore } from "../stores";
import { CurrentUserResponse } from "../models/CurrentUserResponse";
import { SignOutResponse } from "../models/SignOutResponse";
import { SignInResponse } from "../models/SignInResponse";
import { SignInRequest } from "../models/SignInRequest";

@Service()
@Resolver()
export class AuthService {
  constructor(
    @Inject("UserStore") public userStore: UserStore,
    @Inject("UserContext") public userContext: UserContext
  ) {}

  @Query(() => CurrentUserResponse)
  async currentUser(): Promise<CurrentUserResponse> {
    return {
      user: this.userContext.currentUser,
    };
  }

  @Mutation(() => SignInResponse)
  async signIn(@Arg("req") req: SignInRequest): Promise<SignInResponse> {
    const { username, password } = req;

    const user = await this.userStore.find({ username });

    if (!user) {
      return {
        username: {
          errors: ["not found"],
        },
      };
    }
    if (user.password !== password) {
      return {
        password: {
          errors: ["incorrect password"],
        },
      };
    }

    this.userContext.currentUser = user;

    return {
      success: true,
    };
  }

  @Mutation(() => SignOutResponse)
  async signOut(): Promise<SignOutResponse> {
    this.userContext.currentUser = undefined;
    return {
      success: true,
    };
  }
}
