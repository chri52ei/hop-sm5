import {
  Arg,
  InputType,
  Mutation,
  ObjectType,
  Query,
  Resolver,
} from "type-graphql";
import { Inject, Service } from "typedi";
import fetch from "node-fetch";

@ObjectType()
export class DineroCustomer {
  name!: string;
}

@ObjectType()
export class DineroInvoice {}

@ObjectType()
export class DineroTransaction {}

@InputType()
export class CreateDineroCustomerRequest {}

@ObjectType()
export class CreateDineroCustomerResponse {}

@InputType()
export class UpdateDineroCustomerRequest {}

@ObjectType()
export class UpdateDineroCustomerResponse {}

@ObjectType()
export class DineroAuth {}

@Service()
@Resolver()
export class DineroService {
  constructor(@Inject("DineroAuth") private dineroAuth: DineroAuth) {}

  @Query(() => [DineroCustomer])
  async getCustomers(): Promise<DineroCustomer[]> {
    const response = await fetch(
      `${process.env.Dinero_Base_Url}/v1/${process.env.Dinero_Organization_Id}/contacts`,
      {
        method: "get",
        headers: {
          "content-type": "application/json",
          Authorization:
            "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNCODAwQzc3NTNCNUZDMUMwNTVDOTU1RDkzQ0IxODJEMUJCN0IyQUFSUzI1NiIsInR5cCI6ImF0K2p3dCIsIng1dCI6Ik80QU1kMU8xX0J3RlhKVmRrOHNZTFJ1M3NxbyJ9.eyJuYmYiOjE2Mzc3NjA5OTQsImV4cCI6MTYzNzc2NDU5NCwiaXNzIjoiaHR0cHM6Ly9jb25uZWN0LnZpc21hLmNvbSIsImF1ZCI6Imh0dHBzOi8vYXBpLmRpbmVyby5kayIsImNsaWVudF9pZCI6Imlzdl9JVF9TdGFja19hcHMiLCJzdWIiOiJkZmFmNWY0MS05ZDc0LTRkYWQtODAwYi00NDU1MzQ2YjIxYzYiLCJhdXRoX3RpbWUiOjE2Mzc3NTE3NjAsImlkcCI6Ikdvb2dsZSIsImxsdCI6IjE2Mzc1Nzk3OTAiLCJhY3IiOiIyIiwic2lkIjoiNmU0NWExODMtMWQwOC1hNmRkLWNiYzItYjVkZThhYmNhMjQwIiwiaWF0IjoxNjM3NzYwOTk0LCJzY29wZSI6WyJkaW5lcm9wdWJsaWNhcGk6cmVhZCIsImRpbmVyb3B1YmxpY2FwaTp3cml0ZSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJwd2QiXX0.C_9-5wPW__ZUKjaw7qsn9wGGF5xszw7l_gsHoL4QaWB8kpEvRwEZf804Xf5mMAmFU1wUzewuJGLZmfUhiD91s6_V-tT1bKB4Dvi2XuEv7mg5F_fmxWTcZOMCuMs7r4BYAbbJH-yGUfATmK_D9etuGnyuxhKdLVwGk3BWZt0Qxr0az4wdNlmBwSo6kWBYJ2aRh21pu5Szwvvs6NrLgLRkJkp1YhQ2WDoDcQd4oVstBxd2ErQwySsMXX1VGZZfgeI06O6jccgnlbjSRc7NmU9qSa1vQFY03g_imK_I6U2Wxpq2TLkhk3rQTbFAQ4iF-v7-Y5mBoBxADl0Ph7gSL8CzsA",
        },
      }
    );
    return (await response.json()).Collection;
  }

  @Query(() => [DineroTransaction])
  async getTransaction(): Promise<DineroTransaction[]> {
    throw "not implemented";
  }

  @Query(() => [DineroInvoice])
  async getInvoices(): Promise<DineroInvoice[]> {
    throw "not implemented";
  }

  @Mutation(() => CreateDineroCustomerResponse)
  async createCustomer(
    @Arg("req") req: CreateDineroCustomerRequest
  ): Promise<CreateDineroCustomerResponse> {
    throw "not implemented";
  }

  @Mutation(() => UpdateDineroCustomerResponse)
  async updateCustomer(
    @Arg("req") req: UpdateDineroCustomerRequest
  ): Promise<UpdateDineroCustomerResponse> {
    throw "not implemented";
  }
}
