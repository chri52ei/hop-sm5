import { UserContext } from "../contexts";
import { UserStore } from "../stores";
import { Inject, Service } from "typedi";
import { Arg, Mutation, Resolver } from "type-graphql";
import { CreateUserRequest } from "../models/CreateUserRequest";
import { CreateUserResponse } from "../models/CreateUserResponse";
import { DeleteUserRequest } from "../models/DeleteUserRequest";
import { DeleteUserResponse } from "../models/DeleteUserResponse";

@Service()
@Resolver()
export class AdminService {
  constructor(
    @Inject("UserStore") public userStore: UserStore,
    @Inject("UserContext") public userContext: UserContext
  ) {}

  @Mutation(() => CreateUserResponse)
  async createUser(
    @Arg("createUserRequest") req: CreateUserRequest
  ): Promise<CreateUserResponse> {
    const { username, password } = req;

    if (this.userContext.currentUser?.username != "admin") {
      return {
        errors: ["permissions denied"],
      };
    }
    if (username.length < 5) {
      return {
        username: {
          errors: ["too short"],
        },
      };
    }
    if (username.length > 16) {
      return {
        username: {
          errors: ["too long"],
        },
      };
    }
    if ((await this.userStore.find({ username })) != null) {
      return {
        username: {
          errors: ["already taken"],
        },
      };
    }
    if (password.length < 5) {
      return {
        password: {
          errors: ["too short"],
        },
      };
    }
    if (password.length > 16) {
      return {
        password: {
          errors: ["too long"],
        },
      };
    }

    await this.userStore.create({ ...req });

    return {
      success: true,
    };
  }

  async deleteUser(req: DeleteUserRequest): Promise<DeleteUserResponse> {
    if (this.userContext.currentUser?.username != "admin") {
      return {
        errors: ["permission error"],
      };
    }

    await this.userStore.delete({ username: req.username });

    return {
      success: true,
    };
  }
}
