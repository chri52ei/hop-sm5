export class ReepayInvoice {}

export class ReepayTransaction {}

export class ReepayCustomer {
  username!: string;
}

export class ReepayPlan {}

export class ReepaySubscription {}

export class CreateReepayCustomerRequest {}
export class CreateReepayCustomerResponse {}

export class CreatePlanRequest {}
export class CreatePlanResponse {}

export class UpdateCustomerRequest {}
export class UpdateCustomerResponse {}

export class UpdateSubscriptionRequest {}
export class UpdateSubscriptionResponse {}

export class UpdatePlanRequest {}
export class UpdatePlanResponse {}

export class CreateInvoiceForSubscriptionRequest {}
export class CreateInvoiceForSubscriptionResponse {}

export class CreateInvoiceForCustomerRequest {}
export class CreateInvoiceForCustomerResponse {}

export class ReepayService {
  async getCustomers(): Promise<ReepayCustomer[]> {
    throw "not implemented";
  }

  async getInvoices(): Promise<ReepayInvoice[]> {
    throw "not implemented";
  }

  async getTransactions(): Promise<ReepayTransaction[]> {
    throw "not implemented";
  }

  async getPlans(): Promise<ReepayPlan[]> {
    throw "not implemented";
  }

  async createCustomer(
    req: CreateReepayCustomerRequest
  ): Promise<CreateReepayCustomerResponse> {
    throw "not implemented";
  }

  async createSubscription(
    req: CreateReepayCustomerRequest
  ): Promise<CreateReepayCustomerResponse> {
    throw "not implemented";
  }

  async createPlan(req: CreatePlanRequest): Promise<CreatePlanResponse> {
    throw "not implemented";
  }

  async updateCustomer(
    req: UpdateCustomerRequest
  ): Promise<UpdateCustomerResponse> {
    throw "not implemented";
  }

  async updateSubscription(
    req: UpdateSubscriptionRequest
  ): Promise<UpdateSubscriptionResponse> {
    throw "not implemented";
  }

  async updatePlan(req: UpdatePlanRequest): Promise<UpdatePlanRequest> {
    throw "not implemented";
  }

  async createInvoiceForSubscription(
    req: CreateInvoiceForSubscriptionRequest
  ): Promise<CreateInvoiceForSubscriptionResponse> {
    throw "not implemented";
  }

  async createInvoiceForCustomer(
    req: CreateInvoiceForCustomerRequest
  ): Promise<CreateInvoiceForCustomerResponse> {
    throw "not implemented";
  }
}
