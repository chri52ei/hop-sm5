import { Field, ObjectType } from "type-graphql";


@ObjectType()
export class SignOutResponse {
  @Field(() => Boolean)
  success?: boolean;
}
