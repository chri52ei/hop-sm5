import { Field, ObjectType } from "type-graphql";
import { ErrorList } from "../../core";

@ObjectType()
export class CreateUserResponse {
  @Field(() => Boolean, { nullable: true })
  success?: boolean;
  @Field(() => [String], { nullable: true })
  errors?: string[];
  @Field(() => ErrorList, { nullable: true })
  username?: ErrorList;
  @Field(() => ErrorList, { nullable: true })
  password?: ErrorList;
}
