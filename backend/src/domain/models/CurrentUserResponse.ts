import { Field, ObjectType } from "type-graphql";
import { User } from "./User";


@ObjectType()
export class CurrentUserResponse {
  @Field(() => User, { nullable: true })
  user?: User | undefined;
}
