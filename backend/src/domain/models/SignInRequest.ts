import { Field, InputType } from "type-graphql";

@InputType()
export class SignInRequest {
  @Field(() => String)
  username!: string;
  @Field(() => String)
  password!: string;
}
