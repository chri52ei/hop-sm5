import { Field, InputType } from "type-graphql";


@InputType()
export class CreateUserRequest {
  @Field()
  username!: string;
  @Field()
  password!: string;
}
;
