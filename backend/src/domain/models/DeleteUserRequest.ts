import { Field, InputType } from "type-graphql";

@InputType()
export class DeleteUserRequest {
  @Field()
  username!: string;

}
