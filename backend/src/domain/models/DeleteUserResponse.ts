import { Field, ObjectType } from "type-graphql";
import { ErrorList } from "../../core";

@ObjectType()
export class DeleteUserResponse {
  @Field(() => Boolean, { nullable: true })
  success?: boolean;

  @Field(() => [ErrorList], { nullable: true })
  errors?: string[];
}
