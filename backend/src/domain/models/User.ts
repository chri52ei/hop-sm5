import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class User {
  @Field()
  username!: string;
  @Field()
  password!: string;
}
