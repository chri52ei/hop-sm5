import { BaseStore } from "../../core";
import { User } from "../models/User";

export type UserStore = BaseStore<User>;
