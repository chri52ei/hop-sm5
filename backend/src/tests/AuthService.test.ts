import "reflect-metadata"
import { User } from "../domain/models/User";
import { AuthService } from "../domain/services/AuthService";
import { MemoryStore } from "../infrastructure/memorystore";

const administrator: User = {
  username: "administrator",
  password: "password",
};

describe("AuthService", () => {
  const userStore = new MemoryStore<User>();
  const authService = new AuthService(userStore, {});

  beforeAll(async () => await userStore.create(administrator));

  it("Can login", async () =>
    authService
      .signIn({ ...administrator })
      .then((res) => expect(res.success).toBe(true)));

  it("Is logged in", async () =>
    authService
      .currentUser()
      .then((res) => expect(res.user).toBe(administrator)));

  it("Can logout", async () =>
    authService.signOut().then((res) => expect(res.success).toBe(true)));

  it("Is logged out", async () =>
    authService.currentUser().then((res) => expect(res.user).toBeUndefined()));

  it("Invalid username returns username error", async () =>
    authService
      .signIn({ username: "john", password: "data" })
      .then((res) => expect(res.username?.errors).toBeDefined()));

  it("Invalid password returns password error", async () =>
    authService
      .signIn({ username: "administrator", password: "testtest" })
      .then((res) => expect(res.password?.errors).toBeDefined()));
});
