import "reflect-metadata"
import { User } from "../domain/models/User";
import { AdminService } from "../domain/services/AdminService";
import { AuthService } from "../domain/services/AuthService";
import { MemoryStore } from "../infrastructure/memorystore";

const administrator: User = {
  username: "admin",
  password: "password",
};

const newUser: User = {
  username: "newuser",
  password: "sadfasdf",
};

describe("AuthService", () => {
  const userStore = new MemoryStore<User>();
  const authService = new AdminService(userStore, {
    currentUser: administrator,
  });

  it("Can create user", async () =>
    authService
      .createUser({ ...newUser })
      .then((res) => expect(res.success).toBe(true)));

  it("New user exists in store", async () =>
    expect(await userStore.find({ username: newUser.username })).toBeDefined());

  it("Can delete user", async () =>
    authService
      .deleteUser(newUser)
      .then((res) => expect(res.success).toBe(true)));

  it("New user no longer exists in store", async () =>
    expect(
      await userStore.find({ username: newUser.username })
    ).toBeUndefined());
});
