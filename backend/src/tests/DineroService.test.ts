import "reflect-metadata";
import { DineroService } from "../domain/services/DineroService";
import * as dotenv from "dotenv";

dotenv.config();

describe("DineroService", () => {
  const dineroService = new DineroService({});

  it("Can get customers", async () =>
    dineroService
      .getCustomers()
      .then((res) => expect(res.length > 0).toBe(true)));

  it("has custer named Martin", async () =>
    dineroService
      .getCustomers()
      .then((res) =>
        expect(
          res.find((customer) => customer.name == "Martin Cevik")
        ).toBeDefined()
      ));
});
