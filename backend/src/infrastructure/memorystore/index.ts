import { BaseStore } from "../../core";

export class MemoryStore<T> implements BaseStore<T> {
  constructor(private items: T[] = []) {}
  findMany(where: Partial<T>): Promise<T[]> {
    throw new Error("Method not implemented.");
  }
  update(where: Partial<T>, action: (obj: T) => void): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  getAll(): T[] {
    return this.items;
  }
  async create(entity: T): Promise<T> {
    this.items.push(entity);
    return entity;
  }
  async delete(where: Partial<T>): Promise<boolean> {
    return this.find(where).then((entity) => {
      if (entity) this.items = this.items.filter((x) => x != entity);
      return true;
    });
  }
  async find(where: Partial<T>): Promise<T | undefined> {
    for (const item of this.items) {
      let match = true;
      for (const [whereKey, whereValue] of Object.entries(where)) {
        for (const [itemKey, itemValue] of Object.entries(item)) {
          if (itemKey == whereKey) {
            if (itemValue != whereValue) {
              match = false;
            }
            break;
          }
        }
      }
      if (match) {
        return item;
      }
    }
    return undefined;
  }
}
