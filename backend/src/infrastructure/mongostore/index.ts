import { BaseStore, Database } from "../../core";
import { Collection, Db, MongoClient, MongoOptions, OptionalId } from "mongodb";

export class MongoDataTable<T> implements BaseStore<T> {
  constructor(private collection: Collection<T>) {}

  async create(entity: T): Promise<T | undefined> {
    return this.collection
      .insertOne(entity as OptionalId<T>)
      .then(() => entity);
  }

  async delete(where: Partial<T>): Promise<boolean> {
    return this.collection.deleteOne(where).then((res) => res.deletedCount > 0);
  }

  async find(where: Partial<T>): Promise<T | undefined> {
    return this.collection
      .findOne(where)
      .then((res) => (res ? res : undefined));
  }

  async findMany(where: Partial<T>): Promise<T[]> {
    return this.collection
      .find(where)
      .toArray()
      .then((arr) => arr as T[]);
  }

  async update(where: Partial<T>, action: (obj: T) => void): Promise<boolean> {
    return this.collection
      .find(where)
      .toArray()
      .then(async (objs) => {
        if (objs.length === 0) return false;

        for (let obj of objs) {
          action(obj);
          await this.collection.updateMany(where, { $set: { ...obj } });
        }
        return true;
      });
  }
}

export class MongoDb implements Database {
  constructor(private mongoDb: Db) {}

  async getTable<Model>(tableName: string): Promise<BaseStore<Model>> {
    return new MongoDataTable<Model>(this.mongoDb.collection(tableName));
  }
}

export async function createMongoDb(
  url: string,
  dbname: string,
  mongoOptions: MongoOptions | undefined = undefined
) {
  const mongoClient = new MongoClient(url, mongoOptions);
  return mongoClient.connect().then(() => new MongoDb(mongoClient.db(dbname)));
}
