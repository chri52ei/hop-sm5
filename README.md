# HOP-SM5

## Opsætning af GitKraken

1) Gå til https://www.gitkraken.com/ og downloader installeren
2) Åbn GitKraken og klik på "On GitLab" i panelet til venstre
3) Klik på "Integrations"->"GitLab"->"Connect to GitLab"
4) Der åbnes et internet vindue - klik accepter, og du burde nu være "Connected" i dit GitKraken vindue.
5) Åbn en ny tab I GitKraken ('+' knappen i panelet øverst i vinduet)
6) Vælg "Clone" tabben, derefter klik på "GitLab.com", vælg en sti at klone til, samt "hop-sm5" i dropdownen, og derefter klik "Clone this repo"

## Sådan starter du projektet i Development mode
....
